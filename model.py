#%%
import numpy as np
#%%

def zero_pad(X, pad):
    return np.pad(X, [(0, 0), (pad, pad), (pad, pad), (0, 0)], mode='constant', constant_values=(0, 0))

def conv_axis_output_shape(n, f, pad, stride):
    return int((n - f + 2 * pad) / stride) + 1

#%%

class Layer():
    def forward(self, X):
        raise NotImplementedError(f'Layer {self.__class__} should implement forward')

    def backward(self):
        raise NotImplementedError(f'Layer {self.__class__} should implement backward')

    def predict(self, X):
        raise NotImplementedError(f'Layer {self.__class__} should implement predict')


#%%

class Conv2D(Layer):
    def __init__(self, in_channels, out_channels, kernel_size, stride, pad):
        self.W = np.random.randn(kernel_size , kernel_size, in_channels, out_channels)
        self.b = np.random.randn(1,1,1,out_channels)
        self.X = None
        self.stride = stride
        self.kernel_size = kernel_size
        self.pad = pad

    def ouptut_shape(self, w, h):
        (wh, ww, _, _) = self.W.shape
        height =  conv_axis_output_shape(h, wh, self.pad, self.stride)
        width = conv_axis_output_shape(w, ww, self.pad, self.stride)
        return height, width

    def f_naive(self, X):
        (m, h, w, c) = X.shape
        (wh, ww, c, wl) = self.W.shape
        (oh, ow) = self.ouptut_shape(w, h)
        pad, stride = self.pad, self.stride
        W, b = self.W, self.b

        Xp = zero_pad(X, pad)
        Z = np.zeros((m, oh, ow, wl))
        for i in range(m):
            for nh in range(oh):
                h0 = nh * stride
                h1 = h0 + wh
                for nw in range(ow):
                    w0 = nw * stride
                    w1 = w0 + ww
                    for l in range(wl):
                        S = Xp[i, h0:h1, w0:w1, :]
                        z = np.sum(S * W[:, :, :, l]) + float(b[:, :, :, l])
                        Z[i, nh, nw, l] = z

        self.X = X
        return Z

    def f_vec(self, X):
        (m, h, w, c) = X.shape
        (wh, ww, c, wl) = self.W.shape
        (oh, ow) = self.ouptut_shape(w, h)
        pad, stride = self.pad, self.stride
        W, b = self.W, self.b

        Xp = zero_pad(X, pad)
        Z = np.zeros((m, oh, ow, wl))
        for i in range(m):
            for nh in range(oh):
                h0 = nh * stride
                h1 = h0 + wh
                for nw in range(ow):
                    w0 = nw * stride
                    w1 = w0 + ww
                    S = Xp[i, h0:h1, w0:w1, :]
                    z = np.sum(S[:, :, :, np.newaxis] * W, (0, 1, 2)) + b
                    Z[i, nh, nw, :] = z
        self.X = X
        return Z

    def forward(self, X):
        (m, h, w, c) = X.shape
        W, b, pad, stride = self.W, self.b, self.pad, self.stride
        (wh, ww, c, wl) = W.shape
        (oh, ow) = self.ouptut_shape(w, h)

        Xp = zero_pad(X, pad)
        Z = np.zeros((m, oh, ow, wl))
        for nh in range(oh):
            h0 = nh * stride
            for nw in range(ow):
                w0 = nw * stride

                S = Xp[:, h0:h0 + wh, w0:w0 + ww, :]
                all_z = S[:, :, :, :, np.newaxis] * W[np.newaxis, :]
                z = np.sum(all_z, (1, 2, 3)) + b
                Z[:, nh, nw, :] = z
        self.X = X
        return Z

    def backward(self, dZ):
        assert self.X is not None, "A forward pass must be done before caling backward"

        X, W, b, f, stride, pad = self.X, self.W, self.b, self.kernel_size, self.stride, self.pad
        (m, zh, zw, zc) = dZ.shape

        dW, db = np.zeros(W.shape), np.zeros(b.shape)
        dX = np.zeros(X.shape)
        Xp, dXp = zero_pad(X, pad), zero_pad(dX, pad)

        for h in range(zh):
            h0 = h * stride
            for w in range(zw):
                w0 = w * stride

                dzwh = dZ[:, h, w, :] # (m, zc)
                dzhw = dzwh[:, np.newaxis, np.newaxis, np.newaxis, :] # (m, 1, 1, 1, zc)
                # (10, f, f, xc) += ((1, f, f, xc, zc) * (m, 1, 1, 1, zc) sum over zc with broadcast on m)
                dXp[:, h0:h0 + f, w0:w0 + f, :] += np.sum(W[np.newaxis, :] * dzhw, axis=4)

                S = Xp[:, h0:h0 + f, w0:w0 + f, :]
                dW += np.sum(S[:, :, :, :, np.newaxis] * dzhw, axis=0)

                db += np.sum(dzhw, axis=0)

        dX = dXp[:, pad:-pad, pad:-pad, :]
        return dX, dW, db

#%%

class MaxPool2D(Layer):

    def __init__(self, shape, stride):
        self.shape = shape
        self.stride = stride
        self.X = None

    def forward(self, X):
        m, xh, xw, xc = X.shape
        stride, shape = self.stride, self.shape

        ah = conv_axis_output_shape(xh, shape, 0, stride)
        aw = conv_axis_output_shape(xw, shape, 0, stride)

        A = np.zeros((m, ah, aw, xc))

        for nh in range(ah):
            h0 = nh * stride
            for nw in range(aw):
                w0 = nw * stride

                S = X[:, h0:h0 + shape, w0:w0 + shape, :]
                A[:, nh, nw, :] = np.max(S, (1, 2))

        self.X = X
        return A

    def backward(self, dA):
        X, f, stride = self.X, self.shape, self.stride
        m, ah, aw, ac = dA.shape

        dX = np.zeros(X.shape)
        for i in range(m):
            for h in range(ah):
                h0 = h * stride
                for w in range(aw):
                    w0 = w * stride
                    for c in range(ac):
                        S = X[i, h0:h0 + f, w0:w0 + f, c]
                        mask = S == np.max(S)
                        dX[i, h0:h0 + f, w0:w0 + f, c] += mask * dA[i, h, w, c]
        return dX

#%%

class Relu(Layer):
    def __init__(self):
        self.A = None

    def forward(self, X):
        A = self.predict(X)
        self.A = A
        return A

    def backward(self, dA):
        return (self.A > 0) * dA

    def predict(self, X):
        return X * (X > 0)

#%%

class Sigmoid(Layer):
    def __init__(self):
        self.A = None

    def forward(self, X):
        A = self.predict(X)
        self.A = A
        return A

    def backward(self, dA):
        return dA * self.A * (1 - self.A)

    def predict(self, X):
        return 1 / (1 + np.exp(-X))

#%%

class Dense(Layer):
    def __init__(self, out_size, activation=Relu):
        self.X = self.W = self.b = self.dW = self.db = None
        self.out_size = out_size
        self.activation = activation()

    def forward(self, X):
        if self.W is None:
            W = np.random.randn(self.out_size, X.shape[0]) * 0.01 # (o, i)
            b = np.zeros((self.out_size, 1))
            self.W, self.b = W, b

        self.X = X
        W, b = self.W, self.b
        Z = np.dot(W, X) + b # (o, i) (i, m) => (o, m) + (m, 1)
        A = self.activation.forward(Z)
        return A

    def backward(self, dA):
        m = dA.shape[1] # (o, m)
        dZ = self.activation.backward(dA) # (o, m)

        dW = 1/m * np.dot(dZ, self.X.T) # (o, m) (m, i)
        db = 1/m * np.sum(dZ, axis=1, keepdims=True)
        dX = np.dot(self.W.T, dZ) # (i, o) (o m) => (i, m)

        self.dW, self.db = dW, db
        return dX

    def predict(self, X):
        Z = np.dot(self.W, X) + self.b
        A = self.activation.predict(Z)
        return A

#%%
# Other costs ? and theyr partial derivatives ?? how to find them

def binary_cross_entropy(AL, Y):
    m = Y.shape[1]
    cost = -1/m * np.sum(np.dot(Y, np.log(AL).T) + np.dot((1-Y), np.log(1-AL).T))
    return np.squeeze(cost)

def binary_cross_entropy_derivative(AL, Y):
    return -(np.divide(Y, AL) - np.divide(1 - Y, 1 - AL))

def accuracy(y1, y2):
    m = y1.shape[1]
    return np.sum(y1 == y2) / m


import tqdm
class Model():
    def __init__(self, layers=None, learning_rate=0.01, epochs=1000):
        assert layers is not None, "Model class need a list of Layer"
        self.layers = layers
        self.epochs = epochs
        self.learning_rate = learning_rate
        self.costs = []

    def fit(self, X, y):
        A = None
        layers = self.layers
        learning_rate = self.learning_rate

        tqdm_bar = tqdm.tqdm(range(self.epochs))
        for _ in tqdm_bar:
            A = X
            for i, l in enumerate(layers):
                A = l.forward(A)

            cost = binary_cross_entropy(A, y)
            self.costs.append(cost)
            tqdm_bar.set_postfix({'cost': cost, 'acc': accuracy(A > 0.5, y)}, refresh=False)

            dA = binary_cross_entropy_derivative(A, y)
            for l in reversed(layers):
                dA = l.backward(dA)

            for l in layers:
                l.W = l.W - (learning_rate * l.dW)
                l.b = l.b - (learning_rate * l.db)

        return self

    def predict(self, X):
        A = X
        for i, l in enumerate(layers):
            A = l.predict(A)
        return A > 0.5

#%%

np.random.seed(42)

# Small dataset for debug
m = 8
X = np.asarray(range(3 * m)).reshape(3, m) / 100
y = (np.sum(X, axis=0) < 0.34) * 1
y = y.reshape(1, -1) # (m,) => (1, m)

# Some larger random dataset seeded
import sklearn.datasets as sk_data
m = 1000
X, y = sk_data.make_classification(n_samples=m)
X = X.T
print(X.shape, y.shape)
y = y.reshape(1, -1) # (m,) => (1, m)
print(y)

from sklearn.model_selection import train_test_split
# sklearn expect (m, n_features) shape, we use (n_features, m) shape
X_train, X_test, y_train, y_test = train_test_split(X.T, y.T, test_size=0.2)
X_train, X_test, y_train, y_test = X_train.T, X_test.T, y_train.T, y_test.T

layers = [
    Dense(10, activation=Relu),
    Dense(10, activation=Relu),
    Dense(1, activation=Sigmoid)
]
model = Model(layers=layers, learning_rate=0.3, epochs=1000)

%time model.fit(X_train, y_train)

costs = model.costs
import seaborn as sns
sns.lineplot(range(len(costs)), costs)

y_pred = model.predict(X_test)
print('test accuracy:', accuracy(y_pred, y_test))

#%%
# Test for vectorized virtual view
X = np.array([i for i in range(4 * 4)])
X = X.reshape(4, 4)


def img_windows_vecs(X, shape, stride):
    print(X.strides)
    X1 = np.lib.stride_tricks.as_strided(X, shape, strides=stride, writeable=False)
    return X1


def view_as_windows(arr_in, window_shape, step):
    # from https://github.com/scikit-image/scikit-image/blob/master/skimage/util/shape.py#L97-L247
    # need to get it work with channels, maybe take a look at other vectorised implementation

    # -- build rolling window view
    slices = tuple(slice(None, None, st) for st in step)
    window_strides = np.array(arr_in.strides)

    indexing_strides = arr_in[slices].strides

    win_indices_shape = (((np.array(arr_in.shape) - np.array(window_shape))
                          // np.array(step)) + 1)

    new_shape = tuple(list(win_indices_shape) + list(window_shape))
    strides = tuple(list(indexing_strides) + list(window_strides))

    arr_out = np.lib.stride_tricks.as_strided(arr_in, shape=new_shape, strides=strides)
    return arr_out


# display(X)
# X1 = img_windows_vecs(X, (4, 2, 2), (16, 32, 8))
# X1.reshape(4, 4)

# view_as_windows(X, (2, 2), (1, 2))
# X1.reshape(4, 4)
# view_as_windows(X, (2, 2), 2)
# shape [4, * 4]

#%%
import unittest

def init_conv_test():
    np.random.seed(1)
    X = np.random.randn(10,5,7,4)
    conv2d = Conv2D(4, 8, 3, stride=2, pad=1)
    return X, conv2d

X, conv2d = init_conv_test()
print('4 loops')
%time a = conv2d.f_naive(X)
print('3 loops')
%time a = conv2d.f_vec(X)
print('2 loops')
%time a = conv2d.forward(X)

def test_conv_layers(Z):
    mean = 0.6923608807576933
    np.testing.assert_almost_equal(mean, np.mean(Z))
    Z_3_2 = np.asarray([[ 2.63200923,  2.13164981,  3.36457258, -0.7230879 ,  2.05481271,
        3.73037509,  0.20628785, -3.18966671],
    [-1.28912231,  2.27650251,  6.61941931,  0.95527176,  8.25132576,
        2.31329639, 13.00689405,  2.34576051],
    [ 4.12920141,  0.21318702,  9.9232075 , -4.99294761, -4.48905824,
        -7.63882813, -4.32166018, -5.65041454],
    [ 5.67714013,  5.50938037,  8.78183548,  4.76919727,  8.81028368,
        -0.27976052,  5.46932496,  5.86287249]])
    np.testing.assert_almost_equal(Z_3_2, Z[3, 2])

class TestConv2D(unittest.TestCase):
    def test_f_naive(self):
        X, convd2 = init_conv_test()
        test_conv_layers(conv2d.f_naive(X))

    def test_f_vec(self):
        X, convd2 = init_conv_test()
        test_conv_layers(conv2d.f_vec(X))

    def test_forward(self):
        X, convd2 = init_conv_test()
        test_conv_layers(conv2d.forward(X))

    def test_backward(self):
        np.random.seed(1)
        A_prev = np.random.randn(10,4,4,3)
        conv2d = Conv2D(3, 8, 2, stride=2, pad=2)
        Z = conv2d.forward(A_prev)
        dA, dW, db = conv2d.backward(Z)
        np.testing.assert_almost_equal(np.mean(dA), 1.4524377775388078)
        np.testing.assert_almost_equal(np.mean(dW), 1.7269914583139088)
        np.testing.assert_almost_equal(np.mean(db), 7.839232564616923)


import torch
class TestMaxPool2D(unittest.TestCase):
    def test_forward(self):
        np.random.seed(1)
        X = np.random.randn(2, 5, 5, 3)
        maxpool = MaxPool2D(stride=1, shape=3)
        a = maxpool.forward(X)

        pytorch_maxpool2d = torch.nn.MaxPool2d(3, stride=1)
        b = pytorch_maxpool2d(torch.tensor(X.transpose(0, 3, 1, 2))) # We use NHWC pytorch NCHW
        b =  b.numpy().transpose(0, 2, 3, 1) # We use NHWC pytorch NCHW

        np.testing.assert_almost_equal(a, b)

    def test_backward(self):
        np.random.seed(1)
        X = np.random.randn(5, 5, 3, 2)
        mp2d = MaxPool2D(shape=2, stride=1)
        A = mp2d.forward(X)
        dA = np.random.randn(5, 4, 2, 2)
        dX = mp2d.backward(dA)
        expected_dx_1_1 = np.asarray([[0, 0],
        [5.05844394, -1.68282702],
        [ 0, 0]])
        np.testing.assert_almost_equal(dX[1,1], expected_dx_1_1)

        print('mean of dA = ', np.mean(dA))
        print('dA_prev[1,1] = ', dX[1,1])


unittest.main(argv=['ignored'], exit=False)